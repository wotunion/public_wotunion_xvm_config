﻿/**
 * Parameters of the Players Panels ("ears").
 * Параметры панелей игроков ("ушей").
 */
{
  // Enemy spotted status marker format for substitutions in extra fields.
  // Подстановка для дополнительного поля с маркером статуса засвета
  "enemySpottedMarker": {
  },
  // Parameters of the Players Panels ("ears").
  // Параметры панелей игроков ("ушей").
  "playersPanel": {
    // Opacity percentage of the panels. 0 - transparent, 100 - opaque.
    // Прозрачность в процентах ушей. 0 - прозрачные, 100 - не прозрачные.
    "alpha": 60,
    // Opacity percentage of icons in the panels. 0 - transparent ... 100 - opaque.
    // Прозрачность в процентах иконок в ушах. 0 - прозрачные, 100 - не прозрачные.
    "iconAlpha": 100,
    // true - Disable Platoon icons.
    // true - убрать отображение иконки взвода.
    "removeSquadIcon": false,
    // true - disable background of the selected player.
    // true - убрать подложку выбранного игрока.
    "removeSelectedBackground": false,
    // true - Remove the Players Panel mode switcher (buttons for changing size).
    // true - убрать переключатель режимов ушей мышкой.
    "removePanelsModeSwitcher": false,
    // Start panels mode. Possible values: "none", "short", "medium", "medium2", "large".
    // Начальный режим ушей. Возможные значения: "none", "short", "medium", "medium2", "large".
    "startMode": "large",
    // Alternative panels mode. Possible values: null, "none", "short", "medium", "medium2", "large".
    // Альтернативный режим ушей. Возможные значения: null, "none", "short", "medium", "medium2", "large".
    "altMode": null,
    // Display options for Team/Clan logos (see battleLoading.xc).
    // Параметры отображения иконки игрока/клана (см. battleLoading.xc).
    "clanIcon": {
      "show": true,
      "x": 0,
      "y": 6,
      "xr": 0,
      "yr": 6,
      "w": 16,
      "h": 16,
      "alpha": 90
    },
    // Options for the "none" panels - empty panels.
    // Режим ушей "none" - пустые уши.
    "none": {
      // false - disable (отключить)
      "enabled": true,
      // Layout ("vertical" or "horizontal")
      // Размещение ("vertical" - вертикально, или "horizontal" - горизонтально)
      "layout": "vertical",
      // Extra fields.
      // Дополнительные поля.
      "extraFields": {
        "leftPanel": {
          "x": 0, // from left side of screen
          "y": 65,
          "width": 350,
          "height": 25,
          // Set of formats for left panel
          // Набор форматов для левой панели
          // example:
          // "format": [
          //   // simple format (just a text)
          //   "{{nick}}",
          //   "<img src='xvm://res/img/panel-bg-l-{{alive|dead}}.png' width='318' height='28'>",
          //   // extended format
          //   { "x": 20, "y": 10, "borderColor": "0xFFFFFF", "format": "{{nick}}" },
          //   { "x": 200, "src": "xvm://res/contour/{{vehiclename}}.png" }
          // ]
          //
          // types of formats available for extended format:
          //   - MovieClip (for loading image)
          //   - TextField (for writing text and creating rectangles)
          // if "src" field is present, MovieClip format will be used
          // if "src" field is absent, TextField format will be used
          //
          // fields available for extended format:
          //   "src" - resource path ("xvm://res/contour/{{vehiclename}}.png")
          //   "format" - text format (macros allowed)
          //
          // fields available for both MovieClip and TextField formats:
          //   "enabled" - enable/disable field creation (global macros allowed)
          //   "x" - x position (macros allowed)
          //   "y" - y position (macros allowed)
          //   "w" - width (macros allowed)
          //   "h" - height (macros allowed)
          //   "bindToIcon" - if enabled, x position is binded to vehicle icon (default false)
          //   "alpha" - transparency in percents (0..100) (macros allowed)
          //   "rotation" - rotation in degrees (0..360) (macros allowed)
          //   "align" - horizontal alignment ("left", "center", "right")
          //      for left panel default value is "left"
          //      for right panel default value is "right"
          //   "scaleX", "scaleY" - scaling (use negative values for mirroring)
          //
          // fields available for TextField format only:
          //   "valign" - vertical alignment ("top", "center", "bottom")
          //      default value is "top"
          //   "borderColor" - if set, draw border with specified color (macros allowed)
          //   "bgColor" - if set, draw background with specified color (macros allowed)
          //   "antiAliasType" - anti aliasing mode ("advanced" or "normal")
          //   "shadow": {
          //     "distance" (in pixels)
          //     "angle"    (0.0 .. 360.0)
          //     "color"    "0xXXXXXX"
          //     "alpha"    (0.0 .. 1.0)
          //     "blur"     (0.0 .. 255.0)
          //     "strength" (0.0 .. 255.0)
          //    }
          //
          // fields available for MovieClip format only:
          //     "highlight" - highlight icon depending on the player state, default false
          //
          // * all fields are optional
          //
          "formats": []
        },
        "rightPanel": {
          "x": 0, // from right side of screen
          "y": 65,
          "width": 350,
          "height": 25,
          // Set of formats for right panel (extended format supported, see above)
          // Набор форматов для правой панели (поддерживается расширенный формат, см. выше)
          "formats": []
        }
      }
    },
    // Options for the "short" panels - panels with frags and vehicle icon.
    // Режим ушей "short" - короткие уши (фраги и иконка танка).
    "short": {
      // false - disable (отключить)
      "enabled": true,
      // Minimum width of the column, 0-250. Default is 0.
      // Минимальная ширина поля, 0-250. По умолчанию: 0.
      "width": 0,
      // Display format for frags (macros allowed, see macros.txt).
      // Формат отображения фрагов (допускаются макроподстановки, см. macros.txt).
      "fragsFormatLeft": "{{frags}}",
      "fragsFormatRight": "{{frags}}",
      // Extra fields. Each field have size 350x25. Fields are placed one above the other.
      // Дополнительные поля. Каждое поле имеет размер 350x25. Поля располагаются друг над другом.
      // Set of formats for left panel (extended format supported, see above)
      // Набор форматов для левой панели (поддерживается расширенный формат, см. выше)
      "extraFieldsLeft": [],
      // Set of formats for right panel (extended format supported, see above)
      // Набор форматов для правой панели (поддерживается расширенный формат, см. выше)
      "extraFieldsRight": [
        // enemy spotted status marker (see above).
        // маркер статуса засвета (см. выше).
        ${"enemySpottedMarker"}
      ]
    },
    // Options for the "medium" panels - the first of the medium panels.
    // Режим ушей "medium" - первые средние уши в игре.
    "medium": {
      // false - disable (отключить)
      "enabled": true,
      // Minimum width of the player's name column, 0-250. Default is 46.
      // Минимальная ширина поля имени игрока, 0-250. По умолчанию: 46.
      "width": 151,
      // Display format for the left panel (macros allowed, see macros.txt).
      // Формат отображения для левой панели (допускаются макроподстановки, см. macros.txt).
      "formatLeft": "",
      // Display format for the right panel (macros allowed, see macros.txt).
      // Формат отображения для правой панели (допускаются макроподстановки, см. macros.txt).
      "formatRight": "",
      // Display format for frags (macros allowed, see macros.txt).
      // Формат отображения фрагов (допускаются макроподстановки, см. macros.txt).
      "fragsFormatLeft": "",
      "fragsFormatRight": "",
      // Extra fields. Each field have size 350x25. Fields are placed one above the other.
      // Дополнительные поля. Каждое поле имеет размер 350x25. Поля располагаются друг над другом.
      // Set of formats for left panel (extended format supported, see above)
      // Набор форматов для левой панели (поддерживается расширенный формат, см. выше)
      "extraFieldsLeft": [       
        { "x": 29, "y": 0, "valign": "center", "format": "<font size='13'><b><font color='{{squad?#FFB964|{{c:r}}}}'>{{name%.15s~..}}</font> <font color='#FFFFC1'>{{clan}}</b></font>", "alpha": "{{alive?100|50}}", "shadow": { "distance": 0, "angle": 0, "color": "0x000000", "alpha": 0.8, "blur": 2, "strength": 7 } },
        { "x": 180, "align": "center", "valign": "center", "format": "<font size='13' color='{{squad?#FFB964|#FFFFC1}}'><b>{{frags|0}}</b></font>", "alpha": "{{alive?100|40}}", "shadow": {} },
        { "x": 180, "align": "center", "valign": "center", "format": "<font size='13' color='{{tk?#00EAFF|#FFFFFF}}'><b>{{frags|0}}</b></font>", "alpha": "{{alive?100|40}}", "shadow": {} }
],
      // Set of formats for right panel (extended format supported, see above)
      // Набор форматов для правой панели (поддерживается расширенный формат, см. выше)
      "extraFieldsRight": [        
        { "x": 29, "y": 0, "valign": "center", "format": "<font size='13'><b><font color='{{squad?#FFB964|{{c:r}}}}'>{{name%.15s~..}}</font> <font color='#FFFFC1'>{{clan}}</b></font>", "alpha": "{{alive?100|50}}", "shadow": { "distance": 0, "angle": 0, "color": "0x000000", "alpha": 0.8, "blur": 2, "strength": 7 } },
        { "x": 180, "align": "center", "valign": "center", "format": "<font size='13' color='{{squad?#FFB964|#FFFFC1}}'><b>{{frags|0}}</b></font>", "alpha": "{{alive?100|40}}", "shadow": {} },
        { "x": 180, "align": "center", "valign": "center", "format": "<font size='13' color='{{tk?#00EAFF|#FFFFFF}}'><b>{{frags|0}}</b></font>", "alpha": "{{alive?100|40}}", "shadow": {} }
      ]
    },
    // Options for the "medium2" panels - the second of the medium panels.
    // Режим ушей "medium2" - вторые средние уши в игре.
    "medium2": {
      // false - disable (отключить)
      "enabled": true,
      // Maximum width of the vehicle name column, 0-250. Default is 65.
      // Максимальная ширина поля названия танка, 0-250. По умолчанию: 65.
      "width": 225,
      // Display format for the left panel (macros allowed, see macros.txt).
      // Формат отображения для левой панели (допускаются макроподстановки, см. macros.txt).
      "formatLeft": "",
      // Display format for the right panel (macros allowed, see macros.txt).
      // Формат отображения для правой панели (допускаются макроподстановки, см. macros.txt).
      "formatRight": "",
      // Display format for frags (macros allowed, see macros.txt).
      // Формат отображения фрагов (допускаются макроподстановки, см. macros.txt).
      "fragsFormatLeft": "",
      "fragsFormatRight": "",
      // Extra fields. Each field have size 350x25. Fields are placed one above the other.
      // Дополнительные поля. Каждое поле имеет размер 350x25. Поля располагаются друг над другом.
      // Set of formats for left panel (extended format supported, see above)
      // Набор форматов для левой панели (поддерживается расширенный формат, см. выше)
      "extraFieldsLeft": [ 
        { "x": 29, "y": 0, "valign": "center", "format": "<font size='13'><b><font color='{{c:wn8}}'>{{wn8%d|----}}</b></font>", "alpha": "{{alive?100|40}}", "shadow": { "distance": 0, "angle": 0, "color": "0x000000", "alpha": 0.8, "blur": 2, "strength": 7 } },
        { "x": 59, "y": 0, "valign": "center", "format": "<font size='13'><b><font color='{{c:kb}}'>{{kb%2d~k|--k}}</b></font>", "alpha": "{{alive?100|40}}", "shadow": { "distance": 0, "angle": 0, "color": "0x000000", "alpha": 0.8, "blur": 2, "strength": 7 } },
        { "x": 84, "y": 0, "valign": "center", "format": "<font size='13'><b><font color='{{c:rating}}'>{{rating%2d~%|--%}}</b></font>", "alpha": "{{alive?100|40}}", "shadow": { "distance": 0, "angle": 0, "color": "0x000000", "alpha": 0.8, "blur": 2, "strength": 7 } },
        { "x": 114, "y": 0, "valign": "center", "format": "<font size='13'><b><font color='{{squad?#FFB964|#FFFFFF}}'>{{name%.15s~..}}</font> <font color='#FFFFC1'>{{clan}}</b></font>", "alpha": "{{alive?100|50}}", "shadow": { "distance": 0, "angle": 0, "color": "0x000000", "alpha": 0.8, "blur": 2, "strength": 7 } },
        { "x": 265, "align": "center", "valign": "center", "format": "<font size='13' color='{{squad?#FFB964|#FFFFC1}}'><b>{{frags|0}}</b></font>", "alpha": "{{alive?100|40}}", "shadow": {} },
        { "x": 265, "align": "center", "valign": "center", "format": "<font size='13' color='{{tk?#00EAFF|#FFFFFF}}'><b>{{frags|0}}</b></font>", "alpha": "{{alive?100|40}}", "shadow": {} }
			  ],
      // Set of formats for right panel (extended format supported, see above)
      // Набор форматов для правой панели (поддерживается расширенный формат, см. выше)
      "extraFieldsRight": [
        { "x": 29, "y": 0, "valign": "center", "format": "<font size='13'><b><font color='{{c:rating}}'>{{rating%2d~%|--%}}</b></font>", "alpha": "{{alive?100|40}}", "shadow": { "distance": 0, "angle": 0, "color": "0x000000", "alpha": 0.8, "blur": 2, "strength": 7 } },
        { "x": 59, "y": 0, "valign": "center", "format": "<font size='13'><b><font color='{{c:kb}}'>{{kb%2d~k|--k}}</b></font>", "alpha": "{{alive?100|40}}", "shadow": { "distance": 0, "angle": 0, "color": "0x000000", "alpha": 0.8, "blur": 2, "strength": 7 } },
        { "x": 84, "y": 0, "valign": "center", "format": "<font size='13'><b><font color='{{c:wn8}}'>{{wn8%d|----}}</b></font>", "alpha": "{{alive?100|40}}", "shadow": { "distance": 0, "angle": 0, "color": "0x000000", "alpha": 0.8, "blur": 2, "strength": 7 } },
        { "x": 114, "y": 0, "valign": "center", "format": "<font size='13'><b><font color='{{squad?#FFB964|#FFFFFF}}'>{{name%.15s~..}}</font> <font color='#FFFFC1'>{{clan}}</b></font>", "alpha": "{{alive?100|50}}", "shadow": { "distance": 0, "angle": 0, "color": "0x000000", "alpha": 0.8, "blur": 2, "strength": 7 } },
        { "x": 265, "align": "center", "valign": "center", "format": "<b>{{frags|0}}</b></font>", "alpha": "{{alive?100|40}}", "shadow": {} }
        // enemy spotted status marker (see above).
        // маркер статуса засвета (см. выше).
       // ${"enemySpottedMarker"}
      ]
    },
    // Options for the "large" panels - the widest panels.
    // Режим ушей "large" - широкие уши в игре.
    "large": {
      // false - disable (отключить)
      "enabled": true,
      // Minimum width of the player's name column, 0-250. Default is 170.
      // Минимальная ширина поля имени игрока, 0-250. По умолчанию: 170.
      "width": 150,
      "width": 225,
      // Display format for player nickname (macros allowed, see macros.txt).
      // Формат отображения имени игрока (допускаются макроподстановки, см. macros.txt).
      "nickFormatLeft": "",
      "nickFormatRight": "",
      // Display format for vehicle name (macros allowed, see macros.txt).
      // Формат отображения названия танка (допускаются макроподстановки, см. macros.txt).
      "vehicleFormatLeft": "",
      "vehicleFormatRight": "",
      // Display format for frags (macros allowed, see macros.txt).
      // Формат отображения фрагов (допускаются макроподстановки, см. macros.txt).
      "fragsFormatLeft": "",
      "fragsFormatRight": "",
      // Extra fields. Each field have size 350x25. Fields are placed one above the other.
      // Дополнительные поля. Каждое поле имеет размер 350x25. Поля располагаются друг над другом.
      // Set of formats for left panel (extended format supported, see above)
      // Набор форматов для левой панели (поддерживается расширенный формат, см. выше)
      "extraFieldsLeft": [ 
        { "x": 29, "y": 0, "valign": "center", "format": "<font size='13'><b><font color='{{c:r}}'>{{r%d|----}}</b></font>", "alpha": "{{alive?100|40}}", "shadow": { "distance": 0, "angle": 0, "color": "0x000000", "alpha": 0.8, "blur": 2, "strength": 7 } },
        { "x": 59, "y": 0, "valign": "center", "format": "<font size='13'><b><font color='{{c:kb}}'>{{kb%2d~k|--k}}</b></font>", "alpha": "{{alive?100|40}}", "shadow": { "distance": 0, "angle": 0, "color": "0x000000", "alpha": 0.8, "blur": 2, "strength": 7 } },
        { "x": 84, "y": 0, "valign": "center", "format": "<font size='13'><b><font color='{{c:rating}}'>{{rating%2d~%|--%}}</b></font>", "alpha": "{{alive?100|40}}", "shadow": { "distance": 0, "angle": 0, "color": "0x000000", "alpha": 0.8, "blur": 2, "strength": 7 } },
        { "x": 114, "y": 0, "valign": "center", "format": "<font size='13'><b><font color='{{squad?#FFB964|#FFFFFF}}'>{{name%.15s~..}}</font> <font color='#FFFFC1'>{{clan}}</b></font>", "alpha": "{{alive?100|50}}", "shadow": { "distance": 0, "angle": 0, "color": "0x000000", "alpha": 0.8, "blur": 2, "strength": 7 } },
        { "x": 265, "align": "center", "valign": "center", "format": "<font size='13' color='{{squad?#FFB964|#FFFFC1}}'><b>{{frags|0}}</b></font>", "alpha": "{{alive?100|40}}", "shadow": {} },
        { "x": 265, "align": "center", "valign": "center", "format": "<font size='13' color='{{tk?#00EAFF|#FFFFFF}}'><b>{{frags|0}}</b></font>", "alpha": "{{alive?100|40}}", "shadow": {} }
        ],
      // Set of formats for right panel (extended format supported, see above)
      // Набор форматов для правой панели (поддерживается расширенный формат, см. выше)
      "extraFieldsRight": [
        { "x": 29, "y": 0, "valign": "center", "format": "<font size='13'><b><font color='{{c:rating}}'>{{rating%2d~%|--%}}</b></font>", "alpha": "{{alive?100|40}}", "shadow": { "distance": 0, "angle": 0, "color": "0x000000", "alpha": 0.8, "blur": 2, "strength": 7 } },
        { "x": 59, "y": 0, "valign": "center", "format": "<font size='13'><b><font color='{{c:kb}}'>{{kb%2d~k|--k}}</b></font>", "alpha": "{{alive?100|40}}", "shadow": { "distance": 0, "angle": 0, "color": "0x000000", "alpha": 0.8, "blur": 2, "strength": 7 } },
        { "x": 84, "y": 0, "valign": "center", "format": "<font size='13'><b><font color='{{c:r}}'>{{r%d|----}}</b></font>", "alpha": "{{alive?100|40}}", "shadow": { "distance": 0, "angle": 0, "color": "0x000000", "alpha": 0.8, "blur": 2, "strength": 7 } },
        { "x": 114, "y": 0, "valign": "center", "format": "<font size='13'><b><font color='{{squad?#FFB964|#FFFFFF}}'>{{name%.15s~..}}</font> <font color='#FFFFC1'>{{clan}}</b></font>", "alpha": "{{alive?100|50}}", "shadow": { "distance": 0, "angle": 0, "color": "0x000000", "alpha": 0.8, "blur": 2, "strength": 7 } },
        { "x": 265, "align": "center", "valign": "center", "format": "<b>{{frags|0}}</b></font>", "alpha": "{{alive?100|40}}", "shadow": {} }
        // enemy spotted status marker (see above).
        // маркер статуса засвета (см. выше).
       // ${"enemySpottedMarker"}
      ]
    }
  }
}
