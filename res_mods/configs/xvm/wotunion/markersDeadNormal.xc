﻿/**
 * Options for dead without Alt markers.
 * Настройки маркеров для трупов без Alt.
 */
{
  // Настройки для союзников.
  "ally": {
          "actionMarker": { "alpha": 100, "visible": true, "x": 0, "y": -67 },
          "clanIcon": { "alpha": 100, "h": 16, "visible": false, "w": 16, "x": 0, "y": -67 },
          "contourIcon": { "alpha": 100, "amount": 0, "color": null, "visible": false, "x": 6, "y": -65 },
          "damageText": {
            "alpha": 100,
            "blowupMessage": "\u002C\u002C\u0025\u002C\u002C",
            "color": "0xFFFFFF",
            "damageMessage": "\u0025",
            "font": { "align": "center", "bold": true, "italic": false, "name": "xvm", "size": 20 },
            "maxRange": 40,
            "shadow": { "alpha": 70, "angle": 45, "color": null, "distance": 0, "size": 5, "strength": 300 },
            "speed": 3,
            "visible": true,
            "x": 0,
            "y": -67
          },
          "damageTextPlayer": {
            "alpha": 100,
            "blowupMessage": "\u005C\u002C\u0025\u002C\u005C",
            "color": "0x00EAFF",
            "damageMessage": "\u005C\u0025\u005C",
            "font": { "align": "center", "bold": true, "italic": false, "name": "xvm", "size": 20 },
            "maxRange": 100,
            "shadow": { "alpha": 70, "angle": 45, "color": "0xFFFFFF", "distance": 0, "size": 3, "strength": 50 },
            "speed": 4,
            "visible": true,
            "x": 0,
            "y": -67
          },
          "damageTextSquadman": {
            "alpha": 100,
            "blowupMessage": "\u002C\u002C\u0106\u002C\u002C",
            "color": "0xFFFFFF",
            "damageMessage": "\u0106",
            "font": { "align": "center", "bold": true, "italic": false, "name": "xvm", "size": 20 },
            "maxRange": 40,
            "shadow": { "alpha": 70, "angle": 45, "color": null, "distance": 0, "size": 3, "strength": 390 },
            "speed": 3,
            "visible": true,
            "x": 0,
            "y": -67
          },
          "healthBar": {
            "alpha": 100,
            "border": { "alpha": 30, "color": "0x000000", "size": 1 },
            "color": null,
            "damage": { "alpha": 80, "color": null, "fade": 1 },
            "fill": { "alpha": 30 },
            "height": 12,
            "lcolor": null,
            "visible": false,
            "width": 80,
            "x": -41,
            "y": -33
          },
          "levelIcon": { "alpha": 100, "visible": false, "x": 0, "y": -21 },
          "textFields": [

          ],
          "vehicleIcon": {
            "alpha": 100,
            "color": null,
            "maxScale": 100,
            "scaleX": 0,
            "scaleY": 16,
            "shadow": { "alpha": 100, "angle": 45, "color": "0x000000", "distance": 0, "size": 6, "strength": 200 },
            "showSpeaker": false,
            "visible": true,
            "x": 0,
            "y": -16
          }
        },
  // Настройки для противников.
  "enemy": {
          "actionMarker": { "alpha": 100, "visible": true, "x": 0, "y": -67 },
          "clanIcon": { "alpha": 100, "h": 16, "visible": false, "w": 16, "x": 0, "y": -67 },
          "contourIcon": { "alpha": 100, "amount": 0, "color": null, "visible": false, "x": 6, "y": -65 },
          "damageText": {
            "alpha": 100,
            "blowupMessage": "\u002C\u002C\u0025\u002C\u002C",
            "color": "0xFFFFFF",
            "damageMessage": "\u0025",
            "font": { "align": "center", "bold": true, "italic": false, "name": "xvm", "size": 20 },
            "maxRange": 40,
            "shadow": { "alpha": 70, "angle": 45, "color": null, "distance": 0, "size": 5, "strength": 250 },
            "speed": 3,
            "visible": true,
            "x": 0,
            "y": -67
          },
          "damageTextPlayer": {
            "alpha": 100,
            "blowupMessage": "\u002C\u002C\u0025\u002C\u002C",
            "color": "0x3300CC",
            "damageMessage": "\u0025",
            "font": { "align": "center", "bold": true, "italic": false, "name": "xvm", "size": 20 },
            "maxRange": 100,
            "shadow": { "alpha": 70, "angle": 45, "color": "0xFFFFFF", "distance": 0, "size": 3, "strength": 200 },
            "speed": 4,
            "visible": true,
            "x": 0,
            "y": -67
          },
          "damageTextSquadman": {
            "alpha": 100,
            "blowupMessage": "\u002C\u002C\u0025\u002C\u002C",
            "color": "0xFFFFFF",
            "damageMessage": "\u0025",
            "font": { "align": "center", "bold": true, "italic": false, "name": "xvm", "size": 20 },
            "maxRange": 40,
            "shadow": { "alpha": 70, "angle": 45, "color": null, "distance": 0, "size": 5, "strength": 390 },
            "speed": 3,
            "visible": true,
            "x": 0,
            "y": -67
          },
          "healthBar": {
            "alpha": 100,
            "border": { "alpha": 30, "color": "0x000000", "size": 1 },
            "color": null,
            "damage": { "alpha": 80, "color": null, "fade": 1 },
            "fill": { "alpha": 30 },
            "height": 12,
            "lcolor": null,
            "visible": false,
            "width": 80,
            "x": -41,
            "y": -33
          },
          "levelIcon": { "alpha": 100, "visible": false, "x": 0, "y": -21 },
          "textFields": [

          ],
          "vehicleIcon": {
            "alpha": 100,
            "color": null,
            "maxScale": 100,
            "scaleX": 0,
            "scaleY": 16,
            "shadow": { "alpha": 100, "angle": 45, "color": "0x000000", "distance": 0, "size": 6, "strength": 200 },
            "showSpeaker": false,
            "visible": true,
            "x": 0,
            "y": -16
          }
        }
}