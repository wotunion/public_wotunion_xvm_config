﻿/**
 * Options for alive without Alt markers.
 * Настройки маркеров для живых без Alt.
 */
{
	"def": {
    	"ratingIndicator": {
              "alpha": "{{a:r}}",
              "color": "{{c:r}}",
              "font": { "align": "center", "bold": false, "italic": false, "name": "xvm", "size": 12 },
              "format": "\u0021",
              "name": "Имя игрока",
              "shadow": { "alpha": "{{a:r}}", "angle": 0, "color": "0x000000", "distance": 0, "size": 1.4, "strength": 200 },
              "visible": true,
              "x": 46,
              "y": -21
        }
  },
  // Настройки для союзников.
  "ally": {
          "actionMarker": { "alpha": 100, "visible": true, "x": 0, "y": -67 },
          "clanIcon": { "alpha": 100, "h": 16, "visible": false, "w": 16, "x": 0, "y": -67 },
          "contourIcon": { "alpha": 100, "amount": 0, "color": null, "visible": false, "x": 6, "y": -65 },
          "damageText": {
            "alpha": 100,
            "blowupMessage": "Уничтожен!",
            "color": "0xFFFFFF",
            "damageMessage": "{{dmg}}",
            "font": { "align": "center", "bold": true, "italic": false, "name": "$FieldFont", "size": 20 },
            "maxRange": 40,
            "shadow": { "alpha": 70, "angle": 45, "color": null, "distance": 0, "size": 5, "strength": 200 },
            "speed": 3,
            "visible": true,
            "x": 0,
            "y": -67
          },
          "damageTextPlayer": {
            "alpha": 100,
            "blowupMessage": "Уничтожен!",
            "color": "0xFFFFFF",
            "damageMessage": "{{dmg}}",
            "font": { "align": "center", "bold": true, "italic": false, "name": "$FieldFont", "size": 20 },
            "maxRange": 40,
            "shadow": { "alpha": 70, "angle": 45, "color": "0x0099FF", "distance": 0, "size": 5, "strength": 300 },
            "speed": 4,
            "visible": true,
            "x": 0,
            "y": -67
          },
          "damageTextSquadman": {
            "alpha": 100,
            "blowupMessage": "Уничтожен!",
            "color": "0xFFFFFF",
            "damageMessage": "{{dmg}}",
            "font": { "align": "center", "bold": true, "italic": false, "name": "$FieldFont", "size": 20 },
            "maxRange": 40,
            "shadow": { "alpha": 70, "angle": 45, "color": "0xFF6600", "distance": 0, "size": 5, "strength": 250 },
            "speed": 4,
            "visible": true,
            "x": 0,
            "y": -67
          },
          "healthBar": {
            "alpha": 100,
            "border": { "alpha": 30, "color": "0x000000", "size": 1 },
            "color": null,
            "damage": { "alpha": 100, "color": "0x00FF00", "fade": 2 },
            "fill": { "alpha": 30 },
            "height": 18,
            "lcolor": null,
            "visible": true,
            "width": 87,
            "x": -42,
            "y": -33
          },
          "levelIcon": { "alpha": 100, "visible": false, "x": 0, "y": -21 },
          "textFields": [
            {
              "alpha": 100,
              "color": null,
              "font": { "align": "center", "bold": false, "italic": false, "name": "Verdana", "size": 13 },
              "format": "{{vehicle}}{{turret}}",
              "name": "Название танка",
              "shadow": { "alpha": 100, "angle": 0, "color": "0x071301", "distance": 0, "size": 6.1, "strength": 250 },
              "visible": true,
              "x": 0,
              "y": -50
            },
            {
              "alpha": "100",
              "color": "0xFFFFFF",
              "font": { "align": "center", "bold": true, "italic": false, "name": "$FieldFont", "size": 14 },
              "format": "{{hp}} / {{hp-max}}",
              "name": "Здоровье",
              "shadow": { "alpha": 100, "angle": 0, "color": "0x000000", "distance": 0, "size": 1.4, "strength": 200 },
              "visible": true,
              "x": 2,
              "y": -18
            },
            {
              "alpha": 100,
              "color": null,
              "font": { "align": "center", "bold": false, "italic": false, "name": "Verdana", "size": 11 },
              "format": "{{nick}}",
              "name": "Имя игрока",
              "shadow": { "alpha": 100, "angle": 0, "color": "0x000000", "distance": 0, "size": 6.1, "strength": 200 },
              "visible": true,
              "x": 0,
              "y": -36
            },
            ${"def.ratingIndicator"}
          ],
          "vehicleIcon": {
            "alpha": 100,
            "color": null,
            "maxScale": 100,
            "scaleX": 0,
            "scaleY": 16,
            "shadow": { "alpha": 100, "angle": 45, "color": "0x000000", "distance": 0, "size": 6, "strength": 200 },
            "showSpeaker": false,
            "visible": true,
            "x": 0,
            "y": -11
          }
        },
  // Настройки для противников.
  "enemy": {
          "actionMarker": { "alpha": 100, "visible": true, "x": 0, "y": -67 },
          "clanIcon": { "alpha": 100, "h": 16, "visible": false, "w": 16, "x": 0, "y": -67 },
          "contourIcon": { "alpha": 100, "amount": 0, "color": null, "visible": false, "x": 6, "y": -65 },
          "damageText": {
            "alpha": 100,
            "blowupMessage": "Уничтожен!",
            "color": "0xFFFFFF",
            "damageMessage": "{{dmg}}",
            "font": { "align": "center", "bold": true, "italic": false, "name": "$FieldFont", "size": 20 },
            "maxRange": 40,
            "shadow": { "alpha": 70, "angle": 45, "color": null, "distance": 0, "size": 5, "strength": 200 },
            "speed": 3,
            "visible": true,
            "x": 0,
            "y": -67
          },
          "damageTextPlayer": {
            "alpha": 100,
            "blowupMessage": "Уничтожен!",
            "color": "0xFFFFFF",
            "damageMessage": "{{dmg}}",
            "font": { "align": "center", "bold": true, "italic": false, "name": "$FieldFont", "size": 20 },
            "maxRange": 40,
            "shadow": { "alpha": 70, "angle": 45, "color": "0x0099FF", "distance": 0, "size": 5, "strength": 300 },
            "speed": 4,
            "visible": true,
            "x": 0,
            "y": -67
          },
          "damageTextSquadman": {
            "alpha": 100,
            "blowupMessage": "Уничтожен!",
            "color": "0xFFFFFF",
            "damageMessage": "{{dmg}}",
            "font": { "align": "center", "bold": true, "italic": false, "name": "$FieldFont", "size": 20 },
            "maxRange": 40,
            "shadow": { "alpha": 70, "angle": 45, "color": "0xFF6600", "distance": 0, "size": 5, "strength": 250 },
            "speed": 4,
            "visible": true,
            "x": 0,
            "y": -67
          },
          "healthBar": {
            "alpha": 100,
            "border": { "alpha": 30, "color": "0x000000", "size": 1 },
            "color": null,
            "damage": { "alpha": 100, "color": "0xFFCC00", "fade": 1 },
            "fill": { "alpha": 30 },
            "height": 18,
            "lcolor": null,
            "visible": true,
            "width": 87,
            "x": -42,
            "y": -33
          },
          "levelIcon": { "alpha": 100, "visible": false, "x": 0, "y": -21 },
          "textFields": [
            {
              "alpha": 100,
              "color": null,
              "font": { "align": "center", "bold": false, "italic": false, "name": "Verdana", "size": 13 },
              "format": "{{vehicle}}{{turret}}",
              "name": "Название танка",
              "shadow": { "alpha": 100, "angle": 0, "color": "0x3D0000", "distance": 0, "size": 1.2, "strength": 550 },
              "visible": true,
              "x": 0,
              "y": -50
            },
            {
              "alpha": "100",
              "color": "0xFFFFFF",
              "font": { "align": "center", "bold": true, "italic": false, "name": "$FieldFont", "size": 14 },
              "format": "{{hp}} / {{hp-max}}",
              "name": "Здоровье",
              "shadow": { "alpha": 100, "angle": 0, "color": "0x3D0000", "distance": 0, "size": 1.2, "strength": 550 },
              "visible": true,
              "x": 2,
              "y": -18
            },
            {
              "alpha": 100,
              "color": null,
              "font": { "align": "center", "bold": false, "italic": false, "name": "Verdana", "size": 11 },
              "format": "{{nick}}",
              "name": "Имя игрока",
              "shadow": { "alpha": 100, "angle": 0, "color": "0x3D0000", "distance": 0, "size": 1.2, "strength": 550 },
              "visible": true,
              "x": 0,
              "y": -36
            },
            ${"def.ratingIndicator"}
          ],
          "vehicleIcon": {
            "alpha": 100,
            "color": null,
            "maxScale": 100,
            "scaleX": 0,
            "scaleY": 16,
            "shadow": { "alpha": 100, "angle": 45, "color": "0x000000", "distance": 0, "size": 6, "strength": 200 },
            "showSpeaker": false,
            "visible": true,
            "x": 0,
            "y": -11
          }
        }
}